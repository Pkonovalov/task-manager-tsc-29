package ru.konovalov.tm.command.user;

import ru.konovalov.tm.enumerated.Role;

import static ru.konovalov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginLockCommand extends AbstractUserCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().lockUserByLogin(nextLine());
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
