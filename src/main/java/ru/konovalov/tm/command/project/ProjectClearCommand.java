package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.model.User;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-clear";
    }

    @Override
    public @NotNull String description() {
        return "Delete all projects";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        User user = serviceLocator.getAuthService().getUser();
        serviceLocator.getProjectService().clear(user.getId());
    }

}

