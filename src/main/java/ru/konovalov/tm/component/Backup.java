package ru.konovalov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.service.IPropertyService;
import ru.konovalov.tm.bootstrap.Bootstrap;
import ru.konovalov.tm.command.domain.BackupLoadCommand;
import ru.konovalov.tm.command.domain.BackupSaveCommand;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;
    private final int interval;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.interval = propertyService.getBackupInterval();
        setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(interval);
        }
    }

    public void save() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

}
