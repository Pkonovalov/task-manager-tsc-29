package ru.konovalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.model.User;

public interface IUserRepository extends IRepository<User> {
    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    boolean existsById(@NotNull String id);

    User removeUser(User user);

    User removeUserById(@Nullable String id);

    User removeUserByLogin(@Nullable String login);

    boolean existsByLogin(@Nullable String login);

    boolean existsByEmail(@Nullable String email);

    void setPasswordById(@NotNull String id, @NotNull String password);
}
