package ru.konovalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    int size();

    @NotNull
    List<E> findAll();

    void add(@Nullable E entity);

    @Nullable
    E findById(@NotNull String id);

    void remove(@Nullable E entity);

    @Nullable
    E removeById(@NotNull String id);
/*
    void addAll(@Nullable List<E> entities);*/

    void addAll(final Collection<E> collection);

    void clear();

/*    @Nullable
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);*/


}
