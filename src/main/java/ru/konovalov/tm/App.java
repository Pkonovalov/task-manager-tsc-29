package ru.konovalov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.bootstrap.Bootstrap;
import ru.konovalov.tm.util.SystemUtil;

public class App {

    public static void main(@Nullable String[] args) {
        System.out.println(SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
}